import gql from "graphql-tag";

const ARTICLES_QUERY2 = gql`
query Articles($start: Int, $sort: String, $where: JSON) {
  articles(limit: 1, start: $start, sort: $sort, where: $where) {
    id
    title
    content
    category {
      id
      name
    }
    image {
      url
    }
  }
}
`;

export default ARTICLES_QUERY2;
