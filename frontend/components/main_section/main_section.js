import React from "react";
import css from "./main_section.scss"

const MainSection = () => {
   return (
      <div className={css.mainSection}>
         <h1 className={css.mainSection__title}>IT solutions to drive your business development</h1>
         <h4 className={css.mainSection__subtitle}>Ensuring digital transformation via processes automation</h4>
      </div>
   );
};

export default MainSection;
