import React from "react";
import Query from "../query/query";
import Link from "next/link";
import css from "./nav.scss"
import CATEGORIES_QUERY from "../../apollo/queries/category/categories";

const Nav = () => {
  return (
    <div className={css.container}>
      <Query query={CATEGORIES_QUERY} id={null}>
        {({ data: { categories } }) => {
          return (
            <div className={css.headerWrapper}>
              <nav className={css.navBar}>
                <div>
                  <ul>
                    <li>
                      <Link href="/">
                        <a href="/"><img src="logo-copy.svg"
                            alt="Logo Processica"
                            className="logo"/>
                        </a>
                      </Link>
                    </li>
                  </ul>
                </div>

                <div className="uk-navbar-right">
                  <ul className="uk-navbar-nav">
                    {categories.map((category, i) => {
                      return (
                        <li key={category.id}>
                          <Link
                            href={{
                              pathname: "category",
                              query: { id: category.id }
                            }}
                          >
                            <a className="uk-link-reset">{category.name}</a>
                          </Link>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </nav>
            </div>
          );
        }}
      </Query>
    </div>
  );
};

export default Nav;
