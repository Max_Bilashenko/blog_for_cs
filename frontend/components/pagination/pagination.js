import React from "react";
import Link from "next/link";
import PropTypes from "prop-types";
import Card from "../card/card";
import css from "./pagination.scss";
import { useQuery } from "@apollo/react-hooks";
import ARTICLES_QUERY2 from "../../apollo/queries/pagination/pagination";
import { useRouter } from "next/router";
import fetch from 'isomorphic-unfetch';
const Articles2 = ({ articles, id }) => {
  // const leftArticlesCount = Math.ceil(articles.length / 5);
  // const leftArticles = articles.slice(0, leftArticlesCount);
  const allArticles = articles;

  const lastPage = Math.ceil(articles.length / 3)
  const router = useRouter();

  const { data, error, loading, fetchMore } = useQuery(ARTICLES_QUERY2, {
    variables: {
      id: id
    }
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {JSON.stringify(error)}</p>;


  return (
<Link href={{ pathname: "articles", query: { } }}>
    <section className={css.articleslist}>
      {allArticles.map((article, i, query) => {
        return <Card article={article} key={`article__${article.id}`} />;
      })}

      <button
        onClick={() => {
          // const { start } = allArticles;
          fetchMore({
            // variables: { after: start },
            updateQuery: (prevResult, { fetchMoreResult }) => {
              fetchMoreResult.articles = [
                ...prevResult.articles,
                ...fetchMoreResult.articles
              ];
              return fetchMoreResult;
            }
          });
        }}
      >
        more
      </button>

      {/* <button onClick={() => router.push(`/article?id=${router.query.id - 1}`)}
        disabled={articles <= 1}>Previous</button>
      <button onClick={() => router.push(`/article?id=${router.query.id + 1}`)}
        disabled={articles >= lastPage}>Next</button> */}

      <div className={css.paginationWrapper}>
        <button onClick={() => router.push(`/articles`)}
          disabled={articles <= 1}>older post</button>

        <button onClick={() => router.push(`/articles?id=1`)}
                  >1</button>

        <button onClick={() => router.push(`/articles?id=2`)}
                          >2</button>

        {/* <button onClick={() => router.push(`/article?id=${router.query.id + 1}`)}
          disabled={articles >= lastPage}>next post</button> */}
      </div>

    </section>
    </Link>
  );
};

export default Articles2;
