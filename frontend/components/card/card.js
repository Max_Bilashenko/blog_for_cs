import React from "react";
import Link from "next/link";
import Moment from "react-moment";
import css from "./card.scss";

const Card = ({ article }) => {
  const imageUrl =
    process.env.NODE_ENV !== "development"
      ? article.image.url
      : process.env.API_URL + article.image.url;
  return (
    <Link href={{ pathname: "article", query: { id: article.id } }}>
      <a className={css.card__body}>
        <img className={css.card__articleImg} src={imageUrl} alt={article.image.url} width="374px" height="230px" />
        <div className={css.articlePreviewInfo}>
          <div className={css.articlePreviewInfo__date}>
                <Moment format="MMM Do, YYYY">{article.published_at}</Moment>
          </div>
          <div id="title" className={css.articlePreviewInfo__title}>
                {article.title}
          </div>
          <div className={css.articlePreviewInfo__text}>
            {article.content.length > 160 ?
              `${article.content.substring(0, 160)}...` : article.content
            }
          </div>
          <div id="category" className={css.articlePreviewInfo__category}>
            {article.category.name}
          </div>
        </div>
      </a>
    </Link>
  );
};

export default Card;
