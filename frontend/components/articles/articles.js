import React from "react";
import PropTypes from "prop-types";
import Card from "../card/card";
import css from "./articles.scss"

const Articles = ({ articles }) => {
  // const leftArticlesCount = Math.ceil(articles.length / 5);
  // const leftArticles = articles.slice(0, leftArticlesCount);
  const allArticles = articles.slice();

  return (
    <div className={css.articleslist}>
      {allArticles.map((article, i) => {
        return <Card article={article} key={`article__${article.id}`} />;
      })}


        {/* <div>
          {leftArticles.map((article, i) => {
            return <Card article={article} key={`article__${article.id}`} />;
          })}
        </div>
        <div> */}  


    </div>
  );
};

export default Articles;
