import React from "react";
import Query from "../query/query";
import Link from "next/link";
import css from "./categories_list.scss";

import CATEGORIES_QUERY from "../../apollo/queries/category/categories";

const CategoriesList = () => {
  return (
    <div className={css.categoriesList}>
      <h6 className={css.categoriesList__title}>Categories</h6>
      <Query query={CATEGORIES_QUERY} id={null}>
        {({ data: { categories } }) => {
          return (
            <ul className={css.categoriesList__wrapper}>
              {categories.map((category, i) => {
                return (
                  <li key={category.id} className={css.categoriesList__item}>
                    <Link
                      href={{
                        pathname: "category",
                        query: { id: category.id }
                      }}
                    >
                      <a className={css.categoriesList__link}>{category.name}</a>
                    </Link>
                  </li>
                );
              })}
            </ul>
          );
        }}
      </Query>
    </div>
  );
};

export default CategoriesList;
