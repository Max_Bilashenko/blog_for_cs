import { useRouter } from "next/router";
import Query from "../components/query/query";
import ReactMarkdown from "react-markdown";
import Moment from "react-moment";
import ARTICLE_QUERY from "../apollo/queries/article/article";
import CategoriesList from "../components/categories_list/categories_list";
import css from "./article.scss";

const Article = () => {
  const router = useRouter();
  return (
    <Query query={ARTICLE_QUERY} id={router.query.id}>
      {({ data: { article } }) => {
        const imageUrl =
          process.env.NODE_ENV !== "development"
            ? article.image.url
            : process.env.API_URL + article.image.url;
        return (
          <div className={css.articleWrapper}>
            <div className={css.mainWrapper}>
              <div className={css.mainSection}>
                <h1 className={css.mainSection__title}>{article.title}</h1>
                <div className={css.container}>

                  <div className={css.articleInfo}>
                    <div className={css.articleInfo__category}>
                      {article.category.name}
                    </div>
                    <div className={css.articleInfo__date}>
                      <Moment format="MMM Do, YYYY">{article.published_at}</Moment>
                    </div>
                  </div>
                </div>
              </div>
              <div className={css.container}>
                <div className={css.contentWrapper}>
                  <section className={css.mainContent}>
                    <div className={css.articleContent}>
                      <ReactMarkdown source={article.content} />
                    </div>
                  </section>
                  <section className={css.sideBar}>
                    <CategoriesList />
                  </section>
                </div>
              </div>
            </div>
          </div>
        );
      }}
    </Query>
  );
};

export default Article;
