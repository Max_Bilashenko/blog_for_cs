
/*

Not for use!!!
This page is not used !!!

*/

import { useRouter } from "next/router";
import Articles from "../components/articles/articles";
import Query from "../components/query/query";
import ARTICLES_QUERY2 from "../apollo/queries/pagination/pagination";

const ArticlesAll = () => {
  const router = useRouter();
  console.log(router.query)
  const page = 1;
  return (
    <div>
    <Query query={ARTICLES_QUERY2} id={null}>
      {({ data: { articles, page=1 } }) => {
        return (
          <div>
            <div className="">
              <div className="">
                <h1>{articles.title}</h1>
                <Articles articles={articles} />
              </div>
            </div>
          </div>
        );
      }}
    </Query>
    <button onClick={() => router.push(`/?page=${page + 1}`)}
    >Next</button>
    </div>
  );
};

export default ArticlesAll;