import React, { useState } from "react";
import Articles from "../components/articles/articles";
import Articles2 from "../components/pagination/pagination";
import Query from "../components/query/query";
import ARTICLES_QUERY from "../apollo/queries/article/articles";
import ARTICLES_QUERY2 from "../apollo/queries/pagination/pagination";
import CategoriesList from "../components/categories_list/categories_list";
import MainSection from "../components/main_section/main_section";
import css from "./base.scss";
import { useRouter } from "next/router";
import ArticlesAll from "./articles-all.js"
// import all from "../components/index"
const Home = () => {
  const router = useRouter();
  return (
    <div className={css.mainWrapper}>
      <MainSection />
      <div className={css.container}>
        <div className={css.contentWrapper}>
          <section className={css.mainContent}>
            <Query query={ARTICLES_QUERY}>
            {({ data: { articles } }) => {
                    return <Articles articles={articles} />;
            }}
          </Query>

            {/* <Query query={ARTICLES_QUERY2} id={router.query.id}>
              {({ data: { articles } }) => {
                return <Articles2 articles={articles} />;
              }}
            </Query> */}
          </section>

          <section className={css.sideBar}>
            <CategoriesList />
          </section>
          {/* <ArticlesAll /> */}
        </div>
      </div>
    </div>
  );
};

export default Home;
