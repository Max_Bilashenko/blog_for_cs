require("dotenv").config();
const withCSS = require("@zeit/next-css");
const withFonts = require("next-fonts");
const withSass = require('@zeit/next-sass');

// module.exports = withCSS({
//   env: {
//     API_URL: process.env.API_URL
//   }
// });

module.exports = withCSS(withSass({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]",
  },
  env: {
    API_URL: process.env.API_URL
  },

  webpack (config, options) {
      config.module.rules.push({
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
          use: {
              loader: 'url-loader',
              options: {
                  limit: 100000
              }
          }
      });

      return config;
  }
}));